/*
 * Copyright (c) 2021 Cocollria
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export function fight_against(Attacker, Defender){
    var damage = 0;
    //攻击方先攻击
    damage = Attacker.attack - Defender.defence;
    if(damage < 0){
        damage = 0;
    }
    Defender.health -= damage;
    //根据防守方的生命值返回结果
    if(Defender.health <= 0){
        Defender.health = 0;
        return true;
    }
    else{
        return false;
    }
}

export function change_position(Attacker, Defender){
    if(Attacker.x - Defender.x > 0){
        Attacker.img_offY = 1;
        Defender.img_offY = 2;
    }
    else if(Attacker.x - Defender.x < 0){
        Attacker.img_offY = 2;
        Defender.img_offY = 1;
    }
    else if(Attacker.y - Defender.y > 0){
        Attacker.img_offY = 3;
        Defender.img_offY = 0;
    }
    else if(Attacker.y - Defender.y < 0){
        Attacker.img_offY = 0;
        Defender.img_offY = 3;
    }
}