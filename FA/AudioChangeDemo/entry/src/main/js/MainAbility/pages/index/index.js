/*
 * Copyright (c) 2021 Shenzhen Kaihong Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import audio from '@ohos.multimedia.audio'

import prompt from '@system.prompt';

import fileio from '@ohos.fileio'

import featureAbility from '@ohos.ability.featureAbility'

var globalThis;
var up = 1;
var down = 1

export default {
    data: {
        title: "",
        bufSize: 1280,
        recorder: null,
        capturer: null,
        audioRenderer: null,
        arrayBufGlobal: null,
        fd: 0,
        path: "",
        display: false,  //显示播放组件
        autoplay: true, // 是否自动播放
        videoId: 'video', // 播放器id
        url: '/data/storage/el2/base/haps/entry/files/audio_record.wav', // 视频地址
        controlShow: true, // 是否显示控制栏
        loop: true, // 是否循环播放
        startTime: 0, // 播放开始时间
        speed: 1, // 播放速度
        isfullscreenchange: true // 是否全屏
    },

    onInit() {
        this.title = this.$t('strings.world');
        let context = featureAbility.getContext();

        //音频采集初始化
        var audioStreamInfo = {
            samplingRate: audio.AudioSamplingRate.SAMPLE_RATE_8000,
            channels: audio.AudioChannel.CHANNEL_1,
            sampleFormat: audio.AudioSampleFormat.SAMPLE_FORMAT_U8,
            encodingType: audio.AudioEncodingType.ENCODING_TYPE_RAW
        }

        var audioCapturerInfo = {
            source: audio.SourceType.SOURCE_TYPE_MIC,
            capturerFlags: 1
        }

        var audioCapturerOptions = {
            streamInfo: audioStreamInfo,
            capturerInfo: audioCapturerInfo
        }
        let that = this;

        audio.createAudioCapturer(audioCapturerOptions,(err, data) => {
            if (err) {
                console.error(`gyf AudioCapturer Created : Error: ${err.message}`);
            }
            else {
                console.info('gyf AudioCapturer Created : Success : SUCCESS');
                that.capturer = data;
            }
        });


        //音频播放初始化
        var audioRendererInfo = {
            content: audio.ContentType.CONTENT_TYPE_SPEECH,
            usage: audio.StreamUsage.STREAM_USAGE_VOICE_COMMUNICATION,
            rendererFlags: 1
        }

        var audioRendererOptions = {
            streamInfo: audioStreamInfo,
            rendererInfo: audioRendererInfo
        }

        audio.createAudioRenderer(audioRendererOptions).then((data) => {
            this.audioRenderer = data;
            console.info('gyf AudioRenderer Created : Success : Stream Type: SUCCESS');
        }).catch((err) => {
            console.info('gyf AudioRenderer Created : ERROR : '+err.message);
        });

        globalThis = this;

    },

    onDestroy() {
        console.log("gyf onDestroy!");
        globalThis.capturer.release((err) => {
            if (err) {
                console.error('gyf capturer release failed');
            } else {
                console.log('gyf capturer released.');
            }
        });

    },

    //变速又变调(+)
    up(data, up) {
        if (1 == up) {
            return data;
        }
        let length = data.byteLength;
        let upLength = Math.round(length / up);
        var upData = new Uint8Array(upLength);
        for (var i = 0, j = 0; i < length; ) {
            if (j >= upLength) {
                break;
            }
            upData[j] = data[i];
            i += up;
            j++;
        }
        return upData;
    },

    //变速又变调(-)
    down(data, down) {
        if (1 == down) {
            return data;
        }

        let length = data.byteLength;
        let downLength = Math.round(length * down);
        var downData = new Uint8Array(downLength);
        for (var i = 0, j = 0; i < length - 1; ) {
            for (var k = 0; k < down; k++) {
                downData[j] = data[i];
                j++;
            }
            i++;
        }
        return downData;
    },

    //变速又变调
    speedUp(data, up) {
        let FRAME_LENGTH = 256;
        if (1 == up) {
            return data;
        }
        let length = data.byteLength;

        let frameShift = FRAME_LENGTH * up;

        let upLength = Math.round(length / up);

        var upData = new Uint8Array(upLength);


        for (var i = 0, j = 0; i < length; ) {
            if (i + FRAME_LENGTH >= length) {
                //arraycopy(data, i, upData, j, length - i)
                let test = data.slice(i, i+length-1);
                for(var k=0; k<test.byteLength; k++) {
                    upData[j+k] = test[k];
                }
                break;
            }

            //arraycopy(data, i, upData, j, FRAME_LENGTH);
            let test1 = data.slice(i, i+FRAME_LENGTH);
            for(var k1=0; k1<test1.byteLength; k1++) {
                upData[j+k1] = test1[k1];
            }

            i += (FRAME_LENGTH + frameShift);
            j += FRAME_LENGTH;
        }
        return upData;
    },

    //变速又变调
    speedDown(data, down) {
        let FRAME_LENGTH = 256;
        if (1 == down) {
            return data;
        }
        let length = data.byteLength;

        let downLength = Math.round(length * down);

        var downData = new Uint8Array(downLength);

        for (var i = 0, j = 0; i < length; ) {
            if (i + FRAME_LENGTH >= length) {
                let lastlength = length - i;
                for (var k = 0; k < down; k++) {
                    //arraycopy(data, lastlength, downData, j, lastlength);
                    let test = data.slice(lastlength, lastlength+lastlength);
                    for(var t=0; t<test.byteLength; t++) {
                        downData[j+t] = test[t];
                    }
                    j += lastlength;
                }
                break;
            }
            for (var k = 0; k < down; k++) {
                //arraycopy(data, i, downData, j, FRAME_LENGTH);
                let test = data.slice(i, i + FRAME_LENGTH);
                for(var t=0; t<test.byteLength; t++) {
                    downData[j+t] = test[t];
                }
                j += FRAME_LENGTH;
            }
            i += FRAME_LENGTH;

        }
        return downData;
    },

    setSpeed(data, up, down) {
        let downData = this.speedDown(data, down);
        let upData = this.speedUp(downData, up);
        return upData;
    },

    setTone(data, up, down) {
        let speedData = this.setSpeed(data, down, up);
        let downData = this.down(speedData, down);
        let upData = this.up(downData, up);
        return upData;
    },

    test(data, up, down) {
        let downData = this.down(data, down);
        let upData = this.up(downData, up);
        return upData;
    },


    //循环调用read，进行数据的读取
    handleBuffer(arrayBuffer) {
        console.log("gyf handleBuffer");

        let result = new Uint8Array(arrayBuffer);
        console.log("gyf handleBuffer ================== " + result);

        let outData = this.test(result, up, down);

        fileio.writeSync(globalThis.fd, outData.buffer);

        globalThis.capturer.read(globalThis.bufSize, true).then(this.handleBuffer);

    },

    getData(bufSize) {
        console.log("gyf getData");
        globalThis.capturer.read(bufSize, true).then(this.handleBuffer);

    },

    getBuf(bufSize) {
        console.log("gyf getBuf");
        this.getData(bufSize);
    },

    writeString(data, offset, str) {
        for (var i = 0; i < str.length; i++) {
            data.setUint8(offset + i, str.charCodeAt(i));
        }
    },

    //假设数据为1000秒钟的时间（8000 * 1000）
    encodeWAV() {
        var dataLen = 8000000;
        var sampleRate = 8000;
        var sampleBits = 8;
        var buffer = new ArrayBuffer(44);
        var data = new DataView(buffer);

        var channelCount = 1;   // 单声道
        var offset = 0;

        // 资源交换文件标识符
        this.writeString(data, offset, 'RIFF'); offset += 4;
        // 下个地址开始到文件尾总字节数,即文件大小-8
        data.setUint32(offset, 36 + dataLen, true); offset += 4;
        // WAV文件标志
        this.writeString(data, offset, 'WAVE'); offset += 4;
        // 波形格式标志
        this.writeString(data, offset, 'fmt '); offset += 4;
        // 过滤字节,一般为 0x10 = 16
        data.setUint32(offset, 16, true); offset += 4;
        // 格式类别 (PCM形式采样数据)
        data.setUint16(offset, 1, true); offset += 2;
        // 通道数
        data.setUint16(offset, channelCount, true); offset += 2;
        // 采样率,每秒样本数,表示每个通道的播放速度
        data.setUint32(offset, sampleRate, true); offset += 4;
        // 波形数据传输率 (每秒平均字节数) 单声道×每秒数据位数×每样本数据位/8
        data.setUint32(offset, channelCount * sampleRate * (sampleBits / 8), true); offset += 4;
        // 快数据调整数 采样一次占用字节数 单声道×每样本的数据位数/8
        data.setUint16(offset, channelCount * (sampleBits / 8), true); offset += 2;
        // 每样本数据位数
        data.setUint16(offset, sampleBits, true); offset += 2;
        // 数据标识符
        this.writeString(data, offset, 'data'); offset += 4;
        // 采样数据总数,即数据总大小-44
        data.setUint32(offset, dataLen, true); offset += 4;

        return data;
    },

    record() {
        console.log("gyf record");
        globalThis.isRecord = true;
        var context = featureAbility.getContext();


        context.getFilesDir().then(function (dir) {
            globalThis.path = dir + "/audio_record.wav";
            console.log("gyf path = " + globalThis.path);
            //打开录音文件
            globalThis.fd = fileio.openSync(globalThis.path, 0o2|0o100|0o1000, 0o666);


            //wav文件写上头部数据 start
            let header = globalThis.encodeWAV();
            console.log("gyf header len = " + header.buffer.byteLength);
            fileio.writeSync(globalThis.fd, header.buffer);
            //wav文件写上头部数据 end


            globalThis.capturer.start().then(function () {
                console.log("gyf start");
                globalThis.capturer.getBufferSize((err, bufferSize) => {
                    if (err) {
                        console.error('gyf getBufferSize error');
                    } else {
                        console.log("gyf bufferSize = " + bufferSize);
                        globalThis.getBuf(bufferSize);
                    }
                });

            });
        });

    },

    recordstop() {
        globalThis.isRecord = false;
        console.log("gyf recordstop");
        globalThis.capturer.stop().then(function (result) {
            console.log("gyf stop result = " + result);
//            globalThis.capturer.release((err) => {
//                if (err) {
//                    console.error('gyf capturer release failed');
//                } else {
//                    console.log('gyf capturer released.');
//                }
//            });
        });
    },

    //2倍速度
    set_8_4() {
        console.log('gyf set_8_4');
        up = 8;
        down = 4;
    },

    //1.75倍速度
    set_7_4() {
        console.log('gyf set_7_4');
        up = 7;
        down = 4;
    },

    //1.5倍速度
    set_6_4() {
        console.log('gyf set_6_4');
        up = 6;
        down = 4;
    },

    //1.25倍速度
    set_5_4() {
        console.log('gyf set_5_4');
        up = 5;
        down = 4;
    },

    //0.75倍速度
    set_3_4() {
        console.log('gyf set_3_4');
        up = 3;
        down = 4;
    },

    //0.5倍速度
    set_2_4() {
        console.log('gyf set_2_4');
        up = 2;
        down = 4;
    },


    // 视频准备完成时触发该事件
    prepared(e) {
        this.showPrompt('时长：' + e.duration + '秒');
        console.log("gyf prepared time1 : " + JSON.stringify(e));
    },
    // 音视频开始播放
    start() {
        this.showPrompt('开始播放');
        console.log("gyf start");
    },
    // 音视频暂停播放
    pause() {
        this.showPrompt('暂停播放');
        console.log("gyf pause");
    },
    // 音视频播放完成
    finish() {
        this.$element('confirmDialog').show();
        console.log("gyf finish");
    },
    // 拖动进度条调用
    seeked(e) {
        this.showPrompt('设置进度：' + e.currenttime + '秒');
        console.log("gyf seeked ");
    },
    // 播放进度变化调用
    timeupdate(e) {

    },

    // dialog确定
    confirm() {
        this.$element('video').start();
        this.$element('confirmDialog').close();
    },
    // dialog取消
    cancel() {
        this.$element('confirmDialog').close();
    },
    // 弹框
    showPrompt(msg) {
        prompt.showToast({
            message: msg,
            duration: 1000
        });
    },
    // 点击视频
    hideControls() {
        this.controlShow = !this.controlShow;
    },

    play() {
        this.display = true;
    },

    playstop() {
        this.display = false;
    }
}
