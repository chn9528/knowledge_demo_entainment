# OpenHarmony 分布式井字过三关小游戏

## 一、简介

#### 1.样例效果

本Demo基于OpenHarmony3.1 Beta，使用eTS语言编写的应用。该样例展示了设备认证，分布式流转，分布式数据管理的能力，新设备通过设备认证后，可以通过分布式流转功能拉起远程设备，通过分布式数据管理能力同步两台设备之间的数据。



新设备需要设备认证(hi3516DV300)

&nbsp;![deviceAuthentication](./resource/deviceAuthentication.gif)

已认证设备不需要二次认证(hi3516DV300)

&nbsp;![startGame](./resource/startGame.gif)





rk3568上面实现的效果

&nbsp;![1](./resource/1.gif)



#### 2.涉及OpenHarmony技术特性

- eTS UI
- 分布式调度
- 分布式数据管理



#### 3.支持OpenHarmony版本

OpenHarmony 3.0 LTS(rk3568无该版本)、OpenHarmony 3.1 Beta

#### 4.支持开发板

- 润和HiSpark Taurus AI Camera(Hi3516d)开发板套件（OpenHarmony 3.0 LTS、OpenHarmony 3.1 Beta）
- 润和大禹系列HH-SCDAYU200开发板套件（OpenHarmony 3.1 Beta）



## 二、 快速上手

#### 1.标准设备环境准备

以润和HiSpark Taurus AI Camera(Hi3516d)开发板套件为例

- [获取OpenHarmony源码](https://gitee.com/openharmony/docs/blob/master/zh-cn/release-notes/OpenHarmony-v3.0.1-LTS.md)OpenHarmony 3.0 LTS
- [获取OpenHarmony源码](https://gitee.com/openharmony/docs/blob/master/zh-cn/release-notes/OpenHarmony-v3.1-beta.md)OpenHarmony 3.1 Beta；
- [源码编译](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-standard-running-rk3568-build.md)
- [开发板烧录](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-standard-running-hi3516-burn.md)

润和大禹系列HH-SCDAYU200开发套件：

- [开发板上新 | RK3568开发板上丝滑体验OpenHarmony标准系统](https://gitee.com/openharmony-sig/knowledge_demo_smart_home/tree/master/dev/docs/rk3568_quick_start)

#### 2.应用编译环境准备

- 下载DevEco Studio [下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio#download_beta)；
- 配置SDK，参考 [配置OpenHarmony-SDK](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md)
- DevEco Studio 点击File -> Open 导入本下面的代码工程TicTacToeGame

#### 3.项目下载和导入

1）git下载

```
git clone https://gitee.com/openharmony-sig/knowledge_demo_temp.git
```

2）项目导入

打开DevEco Studio,点击File->Open->下载路径/FA/Entertainment/TicTacToeGame


#### 4.安装应用

- [配置应用签名信息](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/configuring-openharmony-app-signature.md)

- 安装应用

  打开**OpenHarmony SDK路径 \toolchains** 文件夹下，执行如下hdc_std命令，其中**path**为hap包所在绝对路径。

  ```text
  hdc_std install -r path\entry-debug-standard-ark-signed.hap
  ```

**PS**分布式流转流转时，需要多个开发板，连接同一个wifi或使用网线连接


##  三、关键代码解读

#### 1.目录结构

```
.
├── entry\src\main\ets
│   ├── MainAbility  
│   	├──pages
│   		├──Chess.ets //棋局管理类
│   		├──Fight.ets //对局界面
│   		├──GameMain.ets //游戏主界面
│   		├──RemoteDataManager.ets //分布式数据管理类
│   		├──RemoteDeviceModel.ets //设备认证管理类
│   	├──app.ets //ets应用程序主入口
```

#### 2.日志查看方法

```
hdc_std shell
hilog | grep TicTacToeGame
```

#### 3.关键代码

- UI界面，设备流转：GameMain.ets
- 设备认证管理: RemoteDeviceModel.ets
- 分布式数据管理: RemoteDeviceModel.ets



## 四、参考链接

- [OpenHarmony基于TS扩展的声明式开发范式](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/arkui-ts/Readme-CN.md)
- [OpenHarmony应用接口](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/apis/Readme-CN.md)
- [OpenHarmonyETS开发FAQ](https://gitee.com/Cruise2019/team_x/blob/master/homework/ets_quick_start/ETS%E5%BC%80%E5%8F%91FAQ.md)
- [从零开发井字过三关小游戏](./quick_develop.md)

