 #视频文件裁剪

## 简介

&emsp;&emsp;在OpenHarmony系统整个框架中有很多子系统，其中多媒体子系统是OpenHarmony比较重要的一个子系统，OpenHarmony中集成了ffmpeg的第三方库，多媒体的很多实现都与ffmpeg库有关，媒体文件的处理有很多应用场景，比如音视频的裁剪，音视频的分离等等。有些功能OpenHarmony多媒体子系统没有提供给外部相应的接口，我们可以通过NAPI的机制自己实现一套接口，提供给应用层去调用。

## 效果展示

本文通过实现音视频文件裁剪的功能，给小伙伴熟悉整个流程。以下是效果图
![](images/1.png)

首选通过选择源文件，来设置输入文件的参数，在裁剪设置中设定裁剪的起始时间和结束时间（单位为秒），参数设定完以后，我们点击裁剪按钮，这样系统对源文件进行媒体文件的裁剪，裁剪成功后，会显示播放按钮。

在整个操作过程中，源文件选择模块的播放按钮是对源文件进行播放，裁剪模块的播放按钮是对裁剪后文件的播放，我们可以通过播放观看结果是否正确。

## 目录结构
![](images/2.png)


## 样例系统端说明
&emsp;&emsp;样例涉及两部分代码<br />
本样例运行在OpenHarmony3.1 release的版本中，在Hi3516DV300平台上运行。<br />
第一部分是应用端代码，在MediaCuteDemo目录中，使用DevEco Studio工具进行编译生成hap包，需要将@ohos.myffmpegdemo.d.ts放到OpenHarmony本地的sdk api目录中。<br />
另一部分代码是myffmpegsys目录，这部分代码是在OpenHarmony源码中编译，主要提供napi接口。<br />
1.myffmpegsys作为一个新的子系统集成到OpenHarmony源码中，放置在OpenHarmony源码的根目录下，和foundation在同一目录下。<br />
2.配置build/subsystem\_config.json<br />

![](images/4.png)
<br />

3.配置产品的productdefine/common/products/XXXX.json（其中XXXX对应的设备型号）

![](images/5.png)

4.配置好了我们的子系统以及对应的组件后，就可以重新编译OpenHarmony源码，编译成功后刷机。

## 样例应用端说明
1.需要将应用中MediaCuteDemo\entry\src\main\js\MainAbility\common\video目录下的所有mp4文件通过hdc file send命令拷贝到设备的一下目录中/data/accounts/account_0/appdata/com.example.mediacutedemo/com.example.entry/com.example.entry.MainAbility
2.设置/data/accounts/account_0/appdata/com.example.mediacutedemo/com.example.entry/com.example.entry.MainAbility目录下mp4文件的权限和所有者

##代码下载说明
&emsp;&emsp;由于代码中涉及到大文件，需要git clone以后，再执行git lfs pull 