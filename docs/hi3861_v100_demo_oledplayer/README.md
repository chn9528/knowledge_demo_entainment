# Pegasus智能家居套件样例开发--oled_player

![wifi_iot](https://gitee.com/hihopeorg/hispark-hm-pegasus/raw/master/docs/figures/2.png)

## 一、简介

* 基于HiSpark WiFi IoT套件（Hi3861芯片），板载0.96寸 128x64 分辨率的 OLED屏
* 板端程序使用了[SSD1306 OLED屏驱动库](https://gitee.com/hihopeorg/hispark-hm-pegasus/tree/OpenHarmonySample/sample/12_ssd1306)，用于实现每帧画面的绘制；
* PC端使用了`opencv-python`，用于实现视频解码、画面缩放、二值化和帧数据打包；

### 说明：

###### 适用版本：OpenHarmony1.0.1~3.0

###### OpenHarmony源码下载：https://gitee.com/openharmony/docs/tree/master/zh-cn/release-notes

###### Demo源码下载：https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/dev/team_x/demo_oledplayer

## 二、如何编译

1. 将源码目录复制到openharmony源码的`applications\sample\wifi-iot\app目录下，
2. 修改openharmony源码的`applications\sample\wifi-iot\app\BUILD.gn`文件，将其中的 `features` 改为：

```
    features = [
        "demo_oledplayer:app",
    ]
```

   3.在openharmony源码顶层目录执行：`hb set`、`.`，选择需要编译的版本`wifiiot_hispark_pegasus`

   4.执行`hb build`启动版本构建

#### 报错解决

1. 编译过程中报错：undefined reference to`hi_i2c_init`等几个`hi_i2c_`开头的函数，
   - **原因：** 因为默认情况下，hi3861_sdk中，I2C的CONFIG选项没有打开
   - **解决：**修改`device/hisilicon/hispark_pegasus/sdk_liteos/build/config/usr_config.mk`文件中的`CONFIG_I2C_SUPPORT`行：
     - `# CONFIG_I2C_SUPPORT is not set`修改为`CONFIG_I2C_SUPPORT=y`

## 三、如何运行

网络环境：一个无线热点，一台PC，PC连接在该热点上；

### 准备视频资源

1. 选择准备播放的视频，使用工具将帧率转为10fps；
   * 目前ssd1306库实测的最大帧率为10fps；
   * ffmpeg转换命令：`ffmpeg -i input.mp4 -r 10 output.mp4`
   * ffmpeg在Ubuntu上可以直接试用`sudo apt install ffmpeg`安装，Windows上可以在官网下载二进制包：https://ffmpeg.org/download.html#build-windows
2. 运行命令：`./video2bin.py output.mp4 out.bin`，将视频转为bin文件；
3. video2bin.py 依赖 `opencv-python` 包，使用 `pip install opencv-python` 命令安装；


### 运行程序

1. PC上运行命令：`./bin2stream.py out.bin`，将会启动一个TCP服务器，默认监听`5678`端口
2. 根据热点信息（SSID,PSK）和PC的IP地址，修改`play/net_params.h`文件中的相关参数：
3. 重新编译：`hb build -f`
4. 将重新编译好的固件烧录到WiFi IoT开发板上，并复位设备；
   * 板子启动后，首先会连上你的热点，然后会连接PC上的TCP服务；
   * 然后就可以看到视频的在OLED屏播放了；



## 四、原理介绍

整体为C/S架构：

* 使用TCP传输帧数据，实现了简单的二进制协议：
  * 请求格式：帧ID（4B）；
  * 响应格式：状态码（4B） + 帧数据长度（4B）+ 帧数据（可选）；
* PC上运行服务端，默认监听`5678`端口，使用Python开发；
  * 启动时加载整个bin文件，并将其按照帧数据大小分割，放入一个list中；
  * 客户端请求特定帧时，直接根据下标索引取出对应帧，并发送给客户端；
  * 这样的设计（视频预先转换好），可以保证服务端的响应尽可能快；
* 板上运行客户端；
  * 主动发送帧请求，并接收服务端的回应；
  * 收到帧数据后通过I2C向OLED屏发送帧数据；

