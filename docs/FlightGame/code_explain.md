## 飞行棋小游戏

### 简介

本Demo基于OpenHarmony使用JS语言设计开发，复刻了经典飞行棋的玩法，红、绿、黄、蓝轮流掷骰子移动棋子，争取快速抵达终点，游戏中也实现了同色棋格跳跃位移和异色棋子同格时攻击返回出生点，游戏结束弹出排行榜显示游戏成绩。

![游戏中](resources/%E6%B8%B8%E6%88%8F%E4%B8%AD.gif)

### 工程目录

```
·
├─entry\src\main
│     │  config.json
│     ├─js
│     │  └─MainAbility
│     │      │  app.js
│     │      ├─common
│     │      │     MapData.js   // 棋盘地图
│     │      ├─i18n
│     │      └─pages
│     │          └─flight    // 游戏页面
│     │                  flight.css
│     │                  flight.hml
│     │                  flight.js
│     └─resources   // 静态资源目录
│         ├─base
│         │  ├─element
│         │  │      string.json
│         │  └─media
│         │          logo.png
│         └─rawfile
```

### 新建项目

在DevEco Studio中点击File -> New Project -> [Standard]Empty Ability -> Next，Project type 选择Application，Language选择JS语言，最后点击Finish完成项目创建。

### 正式开发

#### 1、页面构建

![游戏页面](resources/%E6%B8%B8%E6%88%8F%E9%A1%B5%E9%9D%A2.png)

- 左边是飞行记录：用于显示各方击落敌机的数量以及当前的飞行进度。游戏的乐趣不仅仅是比谁最先抵达终点，在行走过程中将敌方棋子击落也是很有意思的事儿。

```
        <div class="flylog">
            <text style="height: 40px; margin-top: 25%; font-size: 20px;">——飞行记录——</text>
            <text style="font-size: 16px; margin: 2%;">阵营  |  击落敌机  |  飞行进度</text>
            <list>
                <list-item style="height: 50px; width: 100%; font-size: 18px;" for="{{ flylog }}">
                    <div style=" justify-content: center; align-items: center; ">
                        <image style="height: 40px; width: 40px; margin-left: 10px;" src="{{ $item.camp }}"></image>
                        <text style="margin-left: 20%; margin-right: 5%; font-size: 22px;">{{ $item.hit }}</text>
                        <text style="margin-left: 20%; margin-right: 5%; font-size: 22px;">{{ $item.progress }}/4</text>
                    </div>
                </list-item>
            </list>
        </div>
```

- 中间飞行棋棋盘：共有16枚棋子，因为在游戏过程中，棋子的位置是动态变化的，所以每个棋子都使用绝对定位，动态赋值相应的left和top属性值，棋子朝向通过transform: rotateZ()动画样式设置图片的旋转角度。同时，棋子是否可移动跟骰子点数和飞机的状态有关，当棋子可移动时对应的disabled属性为false。

```
        <div class="middle">
            <image style="object-fit : contain;" src="common/sky.png"></image>

            <image class="chess" style="left: {{ RED[0].x }}%; top: {{ RED[0].y }}%; transform: rotateZ({{ RED[0].angle }});"
                   src="common/red.png" disabled="{{ RED[0].chess_dab }}" onclick="appoint(RED, 0)"></image>
<!--            ……-->

            <image class="chess" style="left: {{ GREEN[0].x }}%; top: {{ GREEN[0].y }}%; transform: rotateZ({{ GREEN[0].angle }});"
                   src="common/green.png" disabled="{{ GREEN[0].chess_dab }}" onclick="appoint(GREEN, 0)"></image>
<!--            ……-->

            <image class="chess" style="left: {{ YELLOW[0].x }}%; top: {{ YELLOW[0].y }}%; transform: rotateZ({{ YELLOW[0].angle }});"
                   src="common/yellow.png" disabled="{{ YELLOW[0].chess_dab }}" onclick="appoint(YELLOW, 0)"></image>
<!--            ……-->

            <image class="chess" style="left: {{ BLUE[0].x }}%; top: {{ BLUE[0].y }}%; transform: rotateZ({{ BLUE[0].angle }});"
                   src="common/blue.png" disabled="{{ BLUE[0].chess_dab }}" onclick="appoint(BLUE, 0)"></image>
<!--            ……-->
        </div>
```

- 右边是操作区：文本播报显示当前回合轮到哪个阵营，点击骰子获取行动点数和长按重新开始按钮。

```
        <div class="side">
            <button class="btn" onlongpress="restart">长按重新开始</button>
            <text style="margin-top: 27%; margin-left: 5%; font-size: 20px;">{{ roundtitle }}</text>
            <image class="dice" disabled="{{ dice_dab }}" src="common/dice/{{ dice_pic }}.png" onclick="todice"></image>
        </div>
```

![排行榜](resources/%E6%8E%92%E8%A1%8C%E6%A6%9C.png)

- 默认隐藏的游戏结束弹窗：待游戏结束时显示排行榜弹窗，显示排名、阵营图标和用时。

```
    <div class="ranking" show="{{ result }}">
        <text style="width: 100%; height: 15%; text-align: center; margin: 10px;">———游戏排名———</text>
        <list>
            <list-item style="justify-content: center; align-items: center; margin-top: 5px;" for="{{ allrank }}">
                <div style="flex-direction: row;">
                    <stack style="justify-content: center; width: 30%; height: 50px;">
                        <image style="width: 50px; height: 50px;" src="common/rank.png"></image>
                        <text style="font-size: 18px; margin-top: 10%;">{{ $item.rank }}</text>
                    </stack>
                    <div style="width: 20%;">
                        <image style="width: 50px; height: 50px;" src="{{ $item.chess }}"></image>
                    </div>
                    <div style="width: 40%; justify-content: center; align-items: center;">
                        <text style="font-size: 22px;">{{ $item.round }}</text>
                    </div>
                </div>
            </list-item>
        </list>
    </div>
```

#### 2、游戏逻辑

2.1、关于地图

- 设置棋格属性，棋盘上共有96个棋格，需要记录每一格的序号、坐标、角度和颜色，还有该棋格上有哪些棋子。对应的js文件数据格式如下：

```
export let MapData = [
    {
        index: 0,       //格子下标
        x: 85,       //格子X轴
        y: 63,       //格子Y轴
        angle: 270,       // 棋子朝向角度（顺时针方向）
        color: "blue",        //格子颜色
        chess: [],      //是否有棋子
    },
    ……
    {
        index: 95,
        x: 86.45,
        y: 14.1, 
        angle: 180,
        color: "blue",
        chess: [],
    },
]

export default MapData;
```

1. 序号index对应的棋格下标，用于查找棋格；
2. 坐标的x和y分别赋值给行走到该棋格的棋子的left和top属性；
3. angle用于设置该棋格上的棋子的朝向角度；
4. color记录棋格颜色，当棋子行走到同色的棋格时会触发位移事件；
5. 棋子数组chess[]则是用来记录当前回合该棋格上有哪些棋子。若是同色的棋子则数组长度加一；若是异色的则触发踩棋事件，将原数组中的元素清空重置，写入新棋子。

- 路线设计

刚刚已经给棋盘上每一格包括停机场、起点和终点都标上序号，而由于各个阵营的棋子的行走路线是不一样的，所以要先设定好各自对应的航线。

1. 红方是（77-80）→76→0→49→52→57
2. 绿方是（82-85）→81→13→51→0→10→58→63
3. 黄方是（87-90）→86→26→51→0→23→64→69
4. 蓝方是（92-95）→91→39→51→0→36→70→75

分别编排四条航线，具体如下：

```
let Route = [
    [76, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
    30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 52, 53, 54, 55, 56, 57],    // 红线
    [81, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
    40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 58, 59, 60, 61, 62, 63],  // 绿线
    [86, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 0,
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 64, 65, 66, 67, 68, 69], // 黄线
    [91, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
    16, 17,18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 70, 71, 72, 73, 74, 75]  // 蓝线
]
```

2.2、游戏事件

![棋子移动](resources/%E6%B8%B8%E6%88%8F%E4%B8%AD.gif)

- 掷骰子：随机掷出点数1~6，根据骰子点数和该阵营棋子的飞机状态改变对应棋子的disabled属性，若无符合条件的棋子可操作则进行回合轮替。

```
    todice() {
        this.dice_dab = true;
        this.dice_num = Math.floor(Math.random()*6+1);
        console.info("骰子点数为 " + this.dice_num);
        switch(this.dice_num) {
            case 1:
                this.dice_pic = "point1";
                break;
            case 2:
                this.dice_pic = "point2";
                break;
            case 3:
                this.dice_pic = "point3";
                break;
            case 4:
                this.dice_pic = "point4";
                break;
            case 5:
                this.dice_pic = "point5";
                break;
            case 6:
                this.dice_pic = "point6";
                break;
            default:
                console.info("骰子意外出错");
                break;
        }
        // 骰子点数小于6，若有飞行状态的棋子可点击，该回合可操作，否则回合轮替
        if(6 > this.dice_num) {
            var operable = false;
            for(var i=0; i<4; i++) {
                if("flying" == thetype[i].type) {
                    thetype[i].chess_dab = false;
                    operable = true;
                }
            }
            if(false == operable) {
                this.rotate();
            }
            else {}
        }
        // 骰子点数为6，除已到达的棋子都可点击
        else {
            for(var i=0; i<4; i++) {
                if("arrive" != thetype[i].type) {
                    thetype[i].chess_dab = false;
                }
            }
        }
    },
```

- 选择棋子：玩家选择可移动的棋子行动，计算前进几步后退几步，定时执行棋子移动方法。

```
    // 选中棋子行动
    appoint(thecamp, num) {
        console.info("————————————————————————");
        console.info(thecamp[0].color + "阵营" + num + "号棋子移动");
        console.info(JSON.stringify(thecamp[num]));
        for(var i=0; i<4; i++) {
            thecamp[i].chess_dab = true;
        }
        // 若该棋子已进入航线
        if(null != thecamp[num].step) {
            console.info("移动前" + JSON.stringify(MapData[Route[this.theround%4][thecamp[num].step]].chess));
            for(var t=0; t<MapData[Route[this.theround%4][thecamp[num].step]].chess.length; t++) {
                if(thecamp[num].index == MapData[Route[this.theround%4][thecamp[num].step]].chess[t].index) {
                    MapData[Route[this.theround%4][thecamp[num].step]].chess.splice(t, 1);
                    console.info("把棋格数组下标为" + t + "的棋子移除，即" + thecamp[num].index);
                    break;
                }
            }
            console.info("移动后" + JSON.stringify(MapData[Route[this.theround%4][thecamp[num].step]].chess));
        }
        // 如果该棋子处于待机状态，进入起点，最后结束
        if("wait" == thecamp[num].type) {
            MapData[thecamp[num].index].chess.pop();
            thecamp[num].step = 0;
            console.info("棋子进入相应航线起点");
            thecamp[num].type = "flying";
            thecamp[num].x = MapData[Route[this.theround%4][thecamp[num].step]].x;
            thecamp[num].y = MapData[Route[this.theround%4][thecamp[num].step]].y;
            thecamp[num].angle = MapData[Route[this.theround%4][thecamp[num].step]].angle;
            MapData[Route[this.theround%4][thecamp[num].step]].chess.push(thecamp[num]);
            console.info("当前格子序号" + Route[this.theround%4][thecamp[num].step]);
            console.info("棋子状态" + JSON.stringify(thecamp[num]));
            this.dice_num = 0;
            this.dice_dab = false;
            this.dice_pic = "dice";
            return;
        }
        temp = this.dice_num;
        // 若走不到终点
        if(56 >= (thecamp[num].step + this.dice_num)) {
            forward = temp;
        }
        // 超过终点，回退几步
        else {
            forward = 56 - thecamp[num].step;
            backward = temp - forward;
        }
        console.info("前进 " + forward + " ———— 后退 " + backward);
        // 0.5秒执行一次走棋方法
        onestep = setInterval(()=> {
            this.move(thecamp[num]);
        }, 500);
    },
```

![游戏结束弹出排行榜](resources/%E6%B8%B8%E6%88%8F%E7%BB%93%E6%9D%9F%E5%BC%B9%E5%87%BA%E6%8E%92%E8%A1%8C%E6%A6%9C.gif)

- 棋子移动：棋子移动骰子点数对应的步数，超过终点时往回退。步数走完确认落点，先后进行踩棋子判定和位移判定，之后进行回合轮替。当有棋子走到终点时更新飞行记录的数据，若有三方阵营已经完成游戏则游戏结束，弹出排行榜，未完成的一方为最后一名。

```
    // 移动棋子
    move(thechess) {
        // 若前进步数为0，且需要后退
        if((0 == forward) && (0 != backward)) {
            thechess.step -= 1;
            backward --;
            console.info("棋子后退一步");
        }
        // 若需要前进
        if(forward != 0) {
            thechess.step += 1;
            forward --;
            console.info("棋子前进一步");
        }
        console.info("棋子行进到第" + thechess.step + "步");
        thechess.x = MapData[Route[this.theround%4][thechess.step]].x;
        thechess.y = MapData[Route[this.theround%4][thechess.step]].y;
        thechess.angle = MapData[Route[this.theround%4][thechess.step]].angle;
        console.info("当前格子序号" + Route[this.theround%4][thechess.step]);
        console.info("棋子状态" + JSON.stringify(thechess));
        temp -= 1;
        // 若步数走完
        if(0 == temp) {
            clearInterval(onestep);
            forward = 0;
            backward = 0;
            this.complex(thechess);     // 踩棋子判断
            this.getjump(thechess);     // 位移判断

            // 向棋子当前落点写入棋子信息
            ruzhan = setTimeout(()=> {
                MapData[Route[this.theround%4][thechess.step]].chess.push(thechess);
                console.info("执行了ruzhan定时器");
            }, 1200);

            // 延迟后进行回合轮替
            changeturn = setTimeout(()=> {
                // 若该棋子到达终点，更新进度
                if(56 == thechess.step) {
                    thechess.type = "arrive";
                    this.flylog[this.theround%4].progress += 1;

                    // 若该棋子走完后刚好全部到达，计入排行榜
                    if(4 == this.flylog[this.theround%4].progress) {
                        console.info("当前完成的队伍数为" + this.allrank.length);
                        this.allrank.push(
                            {
                                rank: this.allrank.length + 1,
                                chess: this.flylog[this.theround%4].camp,
                                round: "用时" + this.theround + "回合",
                            }
                        )
                        if(3 == this.allrank.length) {
                            for(var i=0; i<4; i++) {
                                if(this.flylog[i].progress < 4) {
                                    var chesstemp = this.flylog[i].camp;
                                }
                            }
                            this.allrank.push(
                                {
                                    rank: this.allrank.length + 1,
                                    chess: chesstemp,
                                    round: "未完成",
                                }
                            )
                            console.info("更新后排行榜信息" + JSON.stringify(this.allrank));
                            this.dice_dab = true;
                            this.result = true;
                            return;
                        }
                        console.info("更新后排行榜信息" + JSON.stringify(this.allrank));
                    }
                }
                this.rotate();
            }, 1500);
        }
    },
```

![触发踩棋事件](resources/%E8%A7%A6%E5%8F%91%E8%B8%A9%E6%A3%8B.gif)

- 踩棋事件判定：当棋子落点处已有棋子时判断是否异色，若为同色则无事发生，若为异色，则旧棋子被打回初始停机坪，清空记录数组。

```
    // 落点是否有棋子
    complex(thechess) {
        if(52 > MapData[Route[this.theround%4][thechess.step]].index) {
            if(0 != MapData[Route[this.theround%4][thechess.step]].chess.length) {
                // 我方棋子
                if(thechess.color == MapData[Route[this.theround%4][thechess.step]].chess[0].color) {
                    console.info("同色棋子");
                }
                // 敌方棋子，踩回起点
                else {
                    console.info("异色棋子");
                    for(var i=0; i<MapData[Route[this.theround%4][thechess.step]].chess.length; i++) {
                        console.info("该棋格数组的第" + i + "个棋子");
                        MapData[Route[this.theround%4][thechess.step]].chess[i].type = "wait";
                        console.info("棋子状态为wait");
                        MapData[Route[this.theround%4][thechess.step]].chess[i].step = null;
                        console.info("棋子当前步数为空");
                        MapData[Route[this.theround%4][thechess.step]].chess[i].x =
                        MapData[MapData[Route[this.theround%4][thechess.step]].chess[i].index].x;
                        MapData[Route[this.theround%4][thechess.step]].chess[i].y =
                        MapData[MapData[Route[this.theround%4][thechess.step]].chess[i].index].y;
                        console.info("棋子x,y坐标改变");
                        MapData[Route[this.theround%4][thechess.step]].chess[i].angle =
                        MapData[MapData[Route[this.theround%4][thechess.step]].chess[i].index].angle;
                        console.info("棋子朝向角度改变");
                        this.flylog[this.theround%4].hit += 1;
                    }
                    MapData[Route[this.theround%4][thechess.step]].chess.splice(0, MapData[Route[this.theround%4][thechess.step]].chess.length);
                    console.info("清空数组");
                }
            }
        }
    },
```

![触发位移事件](resources/%E8%A7%A6%E5%8F%91%E4%BD%8D%E7%A7%BB.gif)

- 跳跃位移判定：

```
    // 判断触发位移
    getjump(thechess) {
        // 在进入最后的直航线前的转角前都有可能触发位移
        if(46 >= thechess.step) {
            if(thechess.color == MapData[Route[this.theround%4][thechess.step]].color) {
                if(18 == thechess.step) {
                    thechess.step += 12;
                    console.info("专属航线，飞跃12格");
                }
                else {
                    thechess.step += 4;
                    console.info("踩到同色棋格，飞跃4格");
                }
                console.info(JSON.stringify(thechess));
                console.info("111该棋子当前航线步数" + Route[this.theround%4][thechess.step]);
                console.info("111该棋子所在格子" + JSON.stringify(MapData[Route[this.theround%4][thechess.step]]));
                jump1 = setTimeout(()=> {
                    thechess.x = MapData[Route[this.theround%4][thechess.step]].x;
                    thechess.y = MapData[Route[this.theround%4][thechess.step]].y;
                    thechess.angle = MapData[Route[this.theround%4][thechess.step]].angle;
                    console.info(JSON.stringify(thechess));
                    console.info("222该棋子当前航线步数" + Route[this.theround%4][thechess.step]);
                    console.info("222该棋子所在格子" + JSON.stringify(MapData[Route[this.theround%4][thechess.step]]));
                    // 第二次踩棋子
                    this.complex(thechess);
                    if(18 == thechess.step) {
                        jump2 = setTimeout(()=> {
                            thechess.step += 12;
                            console.info("运气真好，专属航线，再次飞跃12格");
                            thechess.x = MapData[Route[this.theround%4][thechess.step]].x;
                            thechess.y = MapData[Route[this.theround%4][thechess.step]].y;
                            thechess.angle = MapData[Route[this.theround%4][thechess.step]].angle;
                            console.info(JSON.stringify(thechess));
                            console.info("222该棋子当前航线步数" + Route[this.theround%4][thechess.step]);
                            console.info("222该棋子所在格子" + JSON.stringify(MapData[Route[this.theround%4][thechess.step]]));
                            // 第三次踩棋子
                            this.complex(thechess);
                        }, 500);
                    }
                }, 500);
            }
        }
    },
```

- 回合轮替：以 回合数%4 的方式进行回合轮替，若玩家掷出6则可再掷一次。

```
    // 回合轮替
    rotate() {
        // 刚刚是否投出6，是则再来一次，否则回合数加一，进行轮替
        if(6 == this.dice_num) {
            if(4 == this.flylog[this.theround%4].progress) {
                this.theround += 1;
            }
        }
        else {
            this.theround += 1;
        }
        console.info("回合数：" + this.theround);
        this.dice_num = 0;
        this.dice_pic = "dice";
        this.dice_dab = false;

        switch(this.theround % 4) {
            case 0:     // 红的回合
                thetype = this.RED;
                this.roundtitle = "红色方的回合";
                console.info("——————红色方的回合——————");
                break;
            case 1:     // 绿的回合
                thetype = this.GREEN;
                this.roundtitle = "绿色方的回合";
                console.info("——————绿色方的回合——————");
                break;
            case 2:     // 黄的回合
                thetype = this.YELLOW;
                this.roundtitle = "黄色方的回合";
                console.info("——————黄色方的回合——————");
                break;
            case 3:     // 蓝的回合
                thetype = this.BLUE;
                this.roundtitle = "蓝色方的回合";
                console.info("——————蓝色方的回合——————");
                break;
            default:
                console.info("意外出错");
                break;
        }
        // 若该颜色的4枚棋子都已到达终点，直接进行回合轮替
        var win = 0;
        for(var i=0; i<4; i++) {
            if("arrive" == thetype[i].type) {
                win += 1;
            }
        }
        if(4 == win) {
            console.info("该颜色的4枚棋子都已到达终点，直接进行回合轮替");
            this.rotate();
        }
    },
```

![长按重新开始](resources/%E9%95%BF%E6%8C%89%E9%87%8D%E6%96%B0%E5%BC%80%E5%A7%8B.gif)

- 重新开始游戏：为了避免误触，将按钮事件设定为长按触发。点击后重置游戏的各个变量为初始值。

```
    // 重新开始游戏
    restart() {
        console.info("重新开始");
        // 重置游戏其它变量
        clearInterval(onestep);
        temp = 0;
        forward = 0;
        backward = 0;
        clearInterval(walk);
        clearTimeout(jump1);
        clearTimeout(jump2);
        clearTimeout(ruzhan);
        clearTimeout(changeturn);
        this.roundtitle = "";
        this.theround = 0;
        this.dice_pic = "dice";
        this.dice_num = 0;
        this.dice_dab = false;
        this.result = false;
        // 重置地图
        for(var i=0; i<MapData.length; i++) {
            MapData[i].chess = [];
        }
        // 重置飞行记录和排行榜
        for(var j=0; j<4; j++) {
            this.flylog[j].hit = 0;
            this.flylog[j].progress = 0;
        }
        this.allrank = [];
        // 重置棋子
        for(var k=0; k<4; k++) {
            this.RED[k].type = "wait";
            this.RED[k].chess_dab = true;
            this.RED[k].step = null;
            this.RED[k].x = MapData[this.RED[k].index].x;
            this.RED[k].y = MapData[this.RED[k].index].y;
            this.RED[k].angle = MapData[this.RED[k].index].angle;
            this.GREEN[k].type = "wait";
            this.GREEN[k].chess_dab = true;
            this.GREEN[k].step = null;
            this.GREEN[k].x = MapData[this.GREEN[k].index].x;
            this.GREEN[k].y = MapData[this.GREEN[k].index].y;
            this.GREEN[k].angle = MapData[this.GREEN[k].index].angle;
            this.YELLOW[k].type = "wait";
            this.YELLOW[k].chess_dab = true;
            this.YELLOW[k].step = null;
            this.YELLOW[k].x = MapData[this.YELLOW[k].index].x;
            this.YELLOW[k].y = MapData[this.YELLOW[k].index].y;
            this.YELLOW[k].angle = MapData[this.YELLOW[k].index].angle;
            this.BLUE[k].type = "wait";
            this.BLUE[k].chess_dab = true;
            this.BLUE[k].step = null;
            this.BLUE[k].x = MapData[this.BLUE[k].index].x;
            this.BLUE[k].y = MapData[this.BLUE[k].index].y;
            this.BLUE[k].angle = MapData[this.BLUE[k].index].angle;
        }
        // 棋子归位
        for(var l=0; l<4; l++) {
            MapData[77+l].chess.push(this.RED[l]);
            MapData[82+l].chess.push(this.GREEN[l]);
            MapData[87+l].chess.push(this.YELLOW[l]);
            MapData[92+l].chess.push(this.BLUE[l]);
        }
        // 默认红色先手
        thetype = this.RED;
        this.roundtitle = "红色方的回合";
        console.info("——————红色方的回合——————");
    },
```

### 项目下载和导入

项目仓库地址： [https://gitee.com/openharmony-sig/knowledge_demo_entainment/tree/master/FA/FlightGame](https://gitee.com/openharmony-sig/knowledge_demo_entainment/tree/master/FA/FlightGame)

1）git下载

```
git clone https://gitee.com/openharmony-sig/knowledge_demo_entainment.git
```

2）项目导入

打开DevEco Studio,点击File->Open->下载路径/FA/FlightGame

### 约束与限制

#### 1. 设备编译约束

- [九联科技Unionpi Tiger(A311D)开发板，源码编译烧录参考](https://gitee.com/openharmony-sig/device_unionpi)

#### 2. 应用编译约束

- 参考 [应用开发快速入门](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/application-dev/quick-start/Readme-CN.md)

- 集成开发环境：DevEco Studio 3.0.0.601版本,[下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio#download_beta)；

- OpenHarmony SDK 3.0.0.0；